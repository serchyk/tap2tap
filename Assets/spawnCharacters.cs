﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnCharacters : MonoBehaviour {

	public PLAYERS players;

	public List<GameObject> charactersPlayer;
	public List<GameObject> balls;
	public List<GameObject> characters;
	public Transform spawnC;
	public int vibor;
	public AudioClip clip;
	public AudioSource source;

	public bool spawn;

	GameSceneScript gSS;

	void Start(){
		gSS = FindObjectOfType<GameSceneScript> ();
	}
	void FixedUpdate () {
		if (gSS.wonGame) {
			
			for (int j = 0; j < balls.Count; j++) {
				Destroy (balls [j]);
			}
			for (int i = 0; i < charactersPlayer.Count; i++) {
				charactersPlayer [i].GetComponent<playerCharacters> ().speed = 0;
				charactersPlayer [i].GetComponent<playerCharacters> ().anim.SetBool ("cooldown", true);
			}
			Time.timeScale = 0f;
		}
	}
	void Update () {
		if (spawn) 
		{
			GameObject characteres = Instantiate (characters[vibor]);
			characteres.GetComponent<playerCharacters> ().players = players;
			characteres.transform.rotation = spawnC.transform.rotation;
			characteres.transform.position = new Vector2 (spawnC.transform.position.x, spawnC.transform.position.y);
			characteres.transform.SetParent (spawnC);
			characteres.GetComponent<playerCharacters> ().charID = charactersPlayer.Count;
			charactersPlayer.Add (characteres);
			if (PlayerPrefs.GetInt ("sound") == 0)
					source.PlayOneShot (clip, 1f);
			spawn = false;
		}
	}
}
