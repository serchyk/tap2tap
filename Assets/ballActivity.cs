﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

[System.Serializable]
public class Chance
{
	public float Min;
	public float Max;
}

public class ballActivity : MonoBehaviour {

	public Chance chance;
	public PLAYERS players;
	public spawnCharacters spawnChar;
	public string nameObject;
	public Image lockObj;
	public float timerAiSpawn;

	AI playerAI;

	public TYPECHARACTERTS typeCharacters;

	float speed;


	void Start(){



		playerAI = FindObjectOfType<AI> ();

		if (players == PLAYERS.Player1) 
		{
			spawnChar = GameObject.Find ("CastleZonePlayer1").GetComponent<spawnCharacters>();
		} 
		else if (players == PLAYERS.Player2) 
		{
			spawnChar = GameObject.Find ("CastleZonePlayer2").GetComponent<spawnCharacters>();
		}

		int spawnAiMiss = Random.Range (1, 3);
		if (playerAI != null && spawnAiMiss == 2 && players == PLAYERS.Player2) {
			
			Destroy (gameObject);
			spawnChar.balls.Remove (gameObject);
		}

		if (playerAI != null && players == PLAYERS.Player2) {
			timerAiSpawn = Random.Range (3f, 7f);
		}
		speed = Random.Range (4f, 7f);
	}
	// Update is called once per frame
	void Update () {

		if (playerAI != null && players == PLAYERS.Player2) {
			timerAiSpawn -= Time.deltaTime;
			if (timerAiSpawn <= 0)
				activityObject ();
		}

		if (players == PLAYERS.Player1) 
		{
			gameObject.transform.position = new Vector2 (gameObject.transform.position.x + speed, gameObject.transform.position.y);
		} else 
			if (players == PLAYERS.Player2 && playerAI == null) 
		{
			gameObject.transform.position = new Vector2 (gameObject.transform.position.x - speed, gameObject.transform.position.y);
		}
			
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "OpenZone") {
			lockObj.enabled = false;
		}
		if (other.gameObject.tag == "DeadZone") {
			spawnChar.balls.Remove (gameObject);
			Destroy (gameObject);
		}
	}
	public void activityObject(){
		if (gameObject.tag == "ballChar") 
		{
			spawnChar.vibor = (int)typeCharacters;
			spawnChar.spawn = true;
			Debug.Log (typeCharacters.ToString ());
			spawnChar.balls.Remove (gameObject);
		}

		//Debug.Log (nameObject);
		Destroy (gameObject);
	}
}
