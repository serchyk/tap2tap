﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using GoogleMobileAds.Api;

public class GameSceneScript : MonoBehaviour
{

    public GameObject player1;
    public GameObject player2;
    public GameObject wonMenu;
    public GameObject HelpScreen;
    public int roundTimeMin;
    public float roundTimeSec;
    public Transform spawnC;
    public List<GameObject> naimGO;

    public int CoinsUp;

    public bool help;

    public bool wonGame;

    int health;

    int vibor;

    spawnCharacters spawnChar;

    public Text wonTxt;
    public Text timerTxt;

    public AudioClip clip;
    public AudioSource source;
    public bool playBattleMusic;
    public Text coins;

    castleProp cP1;
    castleProp cP2;

    //public string appID = "";
    //public string bannerID = "";
    //public string interstitialID = "";
    //public string videoID = "";
    //public string nativeBannerID = "";
    //public GameObject loadVideo;
    //private RewardBasedVideoAd rewardBasedVideo;
    public bool ads;
    public GameObject needCoins;

    // Use this for initialization
    void Awake()
    {
        if (PlayerPrefs.GetInt("FIRSTTIMEOPENINGGame", 1) == 1)
        {
            HelpScreen.SetActive(true);
            PlayerPrefs.SetInt("FIRSTTIMEOPENINGGame", 0);
            help = true;
        }
        else
        {
            help = false;
            HelpScreen.SetActive(false);
        }
        if (help)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }
    void Start()
    {
        cP1 = player1.GetComponentInChildren<castleProp>();
        cP2 = player2.GetComponentInChildren<castleProp>();
        health = (int)cP1.health / 2;
        spawnChar = GameObject.Find("CastleZonePlayer1").GetComponent<spawnCharacters>();
//#if UNITY_IOS
//        appID="ca-app-pub-3940256099942544~1458002511";
//        bannerID="ca-app-pub-3940256099942544/2934735716";
//        interstitialID="ca-app-pub-3940256099942544/4411468910";
//        videoID="ca-app-pub-3940256099942544/1712485313";
//        nativeBannerID = "ca-app-pub-3940256099942544/3986624511";
//#elif UNITY_ANDROID
//        appID = "ca-app-pub-3940256099942544~3347511713";
//        bannerID = "ca-app-pub-3940256099942544/6300978111";
//        interstitialID = "ca-app-pub-3940256099942544/1033173712";
//        videoID = "ca-app-pub-3940256099942544/5224354917";
//        nativeBannerID = "ca-app-pub-3940256099942544/2247696110";
//#endif


        //// Initialize the Google Mobile Ads SDK.
        //MobileAds.Initialize(appID);


    }

    // Update is called once per frame
    void Update()
    {
        coins.text = CoinsUp.ToString();

        if (help)
        {
            Time.timeScale = 0f;
        }
        if (PlayerPrefs.GetInt("music") == 0)
        {
            if (!playBattleMusic)
            {
                source.PlayOneShot(clip, 1f);
                playBattleMusic = true;
            }
        }
        else
        {
            playBattleMusic = false;
            source.Stop();
        }

        if (roundTimeSec <= 0)
        {
            roundTimeMin--;
            roundTimeSec = 59f;
        }
        if (roundTimeMin >= 0)
        {
            roundTimeSec -= Time.deltaTime;
        }
        //Hard damage Castle P1
        if (cP1.health <= health)
        {
            cP1.gameObject.GetComponent<Animator>().SetBool("damage", true);
        }
        if (cP1.health <= 0)
        {
            cP1.gameObject.GetComponent<Animator>().SetBool("destroy", true);
        }
        //************************\\

        //Hard damage Castle P2
        if (cP2.health <= health)
        {
            cP2.gameObject.GetComponent<Animator>().SetBool("damage", true);
        }
        if (cP2.health <= 0)
        {
            cP2.gameObject.GetComponent<Animator>().SetBool("destroy", true);
        }
        //************************\\

        timerTxt.text = roundTimeMin + ":" + (int)roundTimeSec;

        if (roundTimeMin == 0 && roundTimeSec <= 0)
        {
            if (cP1.health < cP2.health)
            {
                wonTxt.text = "Player 2 WON!!!";
                player1.GetComponentInChildren<nextBall>().won = 1;
                player2.GetComponentInChildren<nextBall>().won = 1;
                wonGame = true;
            }
            else
            {
                wonTxt.text = "Player 1 WON!!!";
                player1.GetComponentInChildren<nextBall>().won = 1;
                player2.GetComponentInChildren<nextBall>().won = 1;
                wonGame = true;
            }
            if (cP1.health == cP2.health)
            {
                wonTxt.text = "Draw";
                player1.GetComponentInChildren<nextBall>().won = 1;
                player2.GetComponentInChildren<nextBall>().won = 1;
                wonGame = true;
            }
            timerTxt.enabled = false;
            wonMenu.SetActive(true);
        }


        if (cP1.health <= 0)
        {
            wonTxt.text = "Player 2 WON!!!";
            timerTxt.enabled = false;
            player1.GetComponentInChildren<nextBall>().won = 1;
            player2.GetComponentInChildren<nextBall>().won = 1;
            wonGame = true;

            wonMenu.SetActive(true);
        }
        if (cP2.health <= 0)
        {
            wonTxt.text = "Player 1 WON!!!";
            timerTxt.enabled = false;
            player1.GetComponentInChildren<nextBall>().won = 1;
            player2.GetComponentInChildren<nextBall>().won = 1;
            wonGame = true;
            wonMenu.SetActive(true);
        }
    }

    public void goToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void goToRestart(int game)
    {
        SceneManager.LoadScene(game);
    }

    public void helpScreen()
    {
        HelpScreen.SetActive(false);
        help = false;
        Time.timeScale = 1.0f;
    }

    public void buyKiller(int naim)
    {
        if (CoinsUp >= naim)
        {
            Debug.Log("куплено");
            CoinsUp -= naim;
            GameObject characteres = Instantiate(naimGO[vibor]);
            characteres.GetComponent<playerCharacters>().players = PLAYERS.Player1;
            characteres.transform.rotation = spawnC.transform.rotation;
            characteres.transform.position = new Vector2(spawnC.transform.position.x, spawnC.transform.position.y);
            characteres.transform.SetParent(spawnC);
            characteres.GetComponent<playerCharacters>().charID = spawnChar.charactersPlayer.Count;
            spawnChar.charactersPlayer.Add(characteres);
            if (PlayerPrefs.GetInt("sound") == 0)
                source.PlayOneShot(clip, 1f);

        }
        else
        {
            //needCoins.SetActive(true);
            Debug.Log("нет денег");
        }
    }
    //public void viewAds()
    //{
    //    ads = false;
    //    loadVideo.SetActive(true);
    //    // Get singleton reward based video ad reference.
    //    this.rewardBasedVideo = RewardBasedVideoAd.Instance;
    //    this.RequestRewardBasedVideo();
    //    // Called when an ad request has successfully loaded.
    //    rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
    //    // Called when an ad request failed to load.
    //    rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
    //    // Called when an ad is shown.
    //    rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
    //    // Called when the ad starts to play.
    //    rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
    //    // Called when the user should be rewarded for watching a video.
    //    rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
    //    // Called when the ad is closed.
    //    rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
    //    // Called when the ad click caused the user to leave the application.
    //    rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
    //}
    public void checkVibor(int f){
        vibor = f;
    }

    //private void RequestRewardBasedVideo()
    //{

    //    // Create an empty ad request.
    //    AdRequest request = new AdRequest.Builder().Build();
    //    // Load the rewarded video ad with the request.
    //    this.rewardBasedVideo.LoadAd(request, videoID);

    //}

    public void exitCoins()
    {
        needCoins.SetActive(false);
    }

    //public void HandleRewardBasedVideoLoaded(object sender, System.EventArgs args)
    //{
    //    rewardBasedVideo.Show();
    //    Debug.Log("loaded");

    //    MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    //}

    //public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    //{
    //    loadVideo.SetActive(false);
    //    Time.timeScale = 1f;
    //    MonoBehaviour.print(
    //        "HandleRewardBasedVideoFailedToLoad event received with message: "
    //        + args.Message);
    //}

    //public void HandleRewardBasedVideoOpened(object sender, System.EventArgs args)
    //{
    //    Time.timeScale = 0f;
    //    MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    //}

    //public void HandleRewardBasedVideoStarted(object sender, System.EventArgs args)
    //{
    //    MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    //}

    //public void HandleRewardBasedVideoClosed(object sender, System.EventArgs args)
    //{
    //    loadVideo.SetActive(false);
    //    Time.timeScale = 1f;
    //    MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    //}

    //public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    //{
        
    //    loadVideo.SetActive(false);
    //    if (!ads)
    //    {
    //        ads = true;
    //        CoinsUp += 100;
    //    }
    //    needCoins.SetActive(false);
    //    //string type = args.Type;
    //    //double amount = args.Amount;
    //    //MonoBehaviour.print(
    //    //  "HandleRewardBasedVideoRewarded event received for "
    //    //  + amount.ToString() + " " + type);
    //}

    //public void HandleRewardBasedVideoLeftApplication(object sender, System.EventArgs args)
    //{
    //    MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    //}
}
