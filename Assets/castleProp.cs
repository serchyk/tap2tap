﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class castleProp : MonoBehaviour {

	public PLAYERS playerCastle;
	public spawnCharacters playerSet;
	public float health = 1000;
	public Image hpCastleBar;

	float maxHealth;

	// Use this for initialization
	void Start () {
		playerCastle = playerSet.players;
		maxHealth = health;
	}
	
	// Update is called once per frame
	void Update () {

		hpCastleBar.fillAmount = health / maxHealth;

		if (playerCastle == PLAYERS.Player1) {
			if (health <= 0) {
				Debug.Log ("Player 2 Won!!!");
			}
		}
		if (playerCastle == PLAYERS.Player2) {
			if (health <= 0) {
				Debug.Log ("Player 1 Won!!!");
			}
		}
	}
}
