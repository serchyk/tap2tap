﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneScript : MonoBehaviour {

	public GameObject menu;
	public GameObject settings;
	public GameObject choiceGame;
	public AudioClip clip;
	public AudioSource source;
	public bool playMusic;
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("music") == 0) {
			if (!playMusic) {
				source.PlayOneShot (clip, 1f);
				playMusic = true;
			}
		} else {
			playMusic = false;
			source.Stop();
		}
	}

	public void goSettings(){
		menu.SetActive (false);
		settings.SetActive (true);
	}

	public void goMenuFromSettings(){
		menu.SetActive (true);
		settings.SetActive (false);
	}

	public void goMenuFromChoice(){
		menu.SetActive (true);
		choiceGame.SetActive (false);
	}

	public void goGame(int choiceGame){
		PlayerPrefs.SetInt ("LevelLoad", choiceGame);
		SceneManager.LoadScene(4);
	}

	public void choice(){
		menu.SetActive (false);
		choiceGame.SetActive (true);
	}
}
