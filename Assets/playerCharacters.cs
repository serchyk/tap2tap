﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PLAYERS
{
	Player1 = 0,
	Player2 = 1
}

public enum TYPECHARACTERTS
{
	Low = 0,
	Medium = 1,
	Hard = 2,
    Naim = 3
}

public class playerCharacters : MonoBehaviour {

	public TYPECHARACTERTS typeCharacters;
	public PLAYERS players;
    public GameSceneScript GSS;
	public int charID;
	public float health;
	float maxHealth;
	public float damage;
	public float speed;
	public float cooldown;
	public bool walk = true;
	public LayerMask NeedLayer;
    public bool ckeckDie;



	public float distance;

	public Image hpBar;

	public Animator anim;

	public GameObject Target;

	public float setCooldown;

	float dieTime = 5f;

	public AudioClip clip;
	public AudioSource source;

    public int addCoin;

	void Start () 
	{
        GSS = FindObjectOfType<GameSceneScript>();
		source = gameObject.GetComponent<AudioSource> ();
		anim = gameObject.GetComponent<Animator> ();

		if (typeCharacters == TYPECHARACTERTS.Low) 
		{
			health = 100f;
			damage = 20f;
			cooldown = 1f;
			speed = 2f;
			distance = Random.Range(40f,49f);
            addCoin = 1;
		}

		if (typeCharacters == TYPECHARACTERTS.Medium) 
		{
			health = 200f;
			damage = 40f;
			speed = 1.5f;
			cooldown = 2f;
			distance = Random.Range(54f,57f);
            addCoin = 5;
		}

		if (typeCharacters == TYPECHARACTERTS.Hard) 
		{
			health = 300f;
			damage = 60f;
			speed = 1f;
			cooldown = 3f;
			distance = Random.Range(64f,69f);
            addCoin = 15;
		}

        if (typeCharacters == TYPECHARACTERTS.Naim)
        {
            health = 500f;
            damage = 80f;
            speed = 1.5f;
            cooldown = 3f;
            distance = Random.Range(64f, 69f);
            addCoin = 40;
        }

		setCooldown = cooldown;
		maxHealth = health;
	}

	void FixedUpdate() 
	{
		//RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.left,35f,8);
		Vector3 forward = transform.TransformDirection(Vector3.right) * distance;

		//RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.zero, 10f, NeedLayer);
		RaycastHit2D hit = Physics2D.Raycast (transform.position + forward, Vector2.zero);
		Debug.DrawRay(transform.position, forward, Color.green);
		if (hit.collider != null) {
			if (hit.collider.gameObject.GetComponent<castleProp> () != null && hit.collider.gameObject.GetComponent<playerCharacters> () == null) {
				//Castle battle
				if (players !=  hit.collider.gameObject.GetComponent<castleProp> ().playerCastle && hit.collider.tag == "Castle") {
					castleProp cP = hit.collider.gameObject.GetComponent<castleProp> ();
					if (cooldown >= setCooldown && hit.collider.gameObject.GetComponent<castleProp> ().health > 0) {
						cP.health -= damage;
						cooldown = 0;
						anim.SetBool ("walk", false);
						anim.SetBool ("attack", true);

						if (PlayerPrefs.GetInt ("sound") == 0)
							source.PlayOneShot (clip, 1f);
					} else {
						anim.SetBool ("attack", false);
					}
					if(Target != null && cooldown < setCooldown){
						
						cooldown += Time.deltaTime;
					}
					walk = false;
					Target = hit.collider.gameObject;
				}
				//******************************\\
			}


			//Debug.Log (hit.transform.tag);
			if (hit.collider.gameObject.GetComponent<playerCharacters> () != null  && health > 0) {
				if (players != hit.collider.gameObject.GetComponent<playerCharacters> ().players && hit.collider.gameObject.tag == "Char" ) {
					playerCharacters pC = hit.collider.gameObject.GetComponent<playerCharacters> ();
					if (cooldown >= setCooldown &&
						hit.collider.gameObject.GetComponent<playerCharacters> ().health > 0) {
						pC.health -= damage;
						cooldown = 0;
						anim.SetBool ("walk", false);
						anim.SetBool ("attack", true);

						if (PlayerPrefs.GetInt ("sound") == 0)
							source.PlayOneShot (clip, 1f);
					} else {
						anim.SetBool ("attack", false);
						cooldown += Time.deltaTime;
					}

					if (pC.Target != gameObject) {
						pC.Target = gameObject;
					}
					walk = false;
					Target = hit.collider.gameObject;
				}
			}

		} else {
			if (cooldown != setCooldown) {
				cooldown += Time.deltaTime;
			}
			if (health > 0) {
					walk = true;
					anim.SetBool ("walk", true);
					anim.SetBool ("attack", false);
			}
		}
	}

	void Update()
	{
		hpBar.fillAmount = health / maxHealth;
		if (players == PLAYERS.Player1)
		{
			
			if (walk) 
			{
				gameObject.transform.position = new Vector2 (gameObject.transform.position.x + speed, gameObject.transform.position.y);
			}
		}

		if (players == PLAYERS.Player2) 
		{
			if (walk) 
			{
				gameObject.transform.position = new Vector2 (gameObject.transform.position.x - speed, gameObject.transform.position.y);
			}
		}

		if (health <= 0) {
			if (dieTime >= 0) {
				if (dieTime == 5) {
					if(typeCharacters == TYPECHARACTERTS.Low)
					transform.position = new Vector2 (transform.position.x, transform.position.y - 15f);

					if(typeCharacters == TYPECHARACTERTS.Medium || typeCharacters == TYPECHARACTERTS.Hard)
						transform.position = new Vector2 (transform.position.x, transform.position.y - 25f);
					
					gameObject.GetComponent<Collider2D> ().enabled = false;
				}
				dieTime -= Time.deltaTime;
                if(players == PLAYERS.Player2 && GSS.coins != null && !ckeckDie){
                    GSS.CoinsUp += addCoin;
                    ckeckDie = true;
                }
				anim.SetBool ("die", true);
			} else
				Destroy (gameObject);
		}
	}
}
