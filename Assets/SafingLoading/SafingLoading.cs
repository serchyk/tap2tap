﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SafingLoading : MonoBehaviour {
	public Image slider;
	public int Level;
	private int slidV;
	private float timer;

	// Use this for initialization
	void Awake () {
		Level = PlayerPrefs.GetInt ("LevelLoad");
	}

	void Start () {
		StartCoroutine (AsyncLoad ());
	}
	
	// Update is called once per frame
	IEnumerator AsyncLoad(){
		AsyncOperation operation = SceneManager.LoadSceneAsync (Level);
		while (operation.isDone) {
			float progress = operation.progress / 0.9f;
			slider.fillAmount = progress;
			yield return null;
		}

	}
}
