﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nextBall : MonoBehaviour {

	public List<GameObject> balls;
	public Transform ballSpawn;

	public GameObject destroyWon;

	public GameObject playersCheck;
	float StopTime = 2f;
	float randomSpawnTime;
	int randomPercent;
	public int won;

	// Update is called once per frame
	void Update () {
		if (StopTime > 0) {
			StopTime -= Time.deltaTime;
		}
		if (won == 0) {
			if (StopTime <= 0) {
				if (randomSpawnTime >= 0)
					randomSpawnTime -= Time.deltaTime;

				if (randomSpawnTime <= 0) {
					for (int i = 0; i < balls.ToArray ().Length; i++) {
						if (i == 0) {
							randomPercent = Random.Range (0, 100);
							randomSpawnTime = 1f;
						}

						if (randomPercent >= balls [i].GetComponent<ballActivity> ().chance.Min &&
						   randomPercent <= balls [i].GetComponent<ballActivity> ().chance.Max) {
							GameObject ball = Instantiate (balls [i]);
							ball.GetComponent<ballActivity> ().players = playersCheck.GetComponent<spawnCharacters> ().players;
							playersCheck.GetComponent<spawnCharacters> ().balls.Add (ball);
							ball.transform.SetParent (ballSpawn);
							ball.transform.position = ballSpawn.transform.position;

						}
					}
				}
			}

		} else
			destroyWon.SetActive (true);
	}
}
