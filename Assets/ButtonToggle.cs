﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour {

	public Image toggleMusicBackground;
	public Image toggleSoundBackground;
	public Toggle tuggleMusicButton;
	public Toggle tuggleSoundButton;

	private void Start()
	{
		if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
		{
			tuggleMusicButton.isOn = true;
			tuggleSoundButton.isOn = true;
			PlayerPrefs.SetInt("FIRSTTIMEOPENING", 0);
		}
		else
		{
			//Music
			if (PlayerPrefs.GetInt ("music") == 1) {
				tuggleMusicButton.isOn = false;
				toggleMusicBackground.enabled = false;
			} else
				tuggleMusicButton.isOn = true;
			//Sound
			if (PlayerPrefs.GetInt ("sound") == 1) {
				tuggleSoundButton.isOn = false;
				toggleSoundBackground.enabled = false;
			} else
				tuggleSoundButton.isOn = true;
		}

	}

	// Update is called once per frame
	void Update () {
		//Music
		if (!tuggleMusicButton.isOn)
		{
			PlayerPrefs.SetInt("music", 1);
			toggleMusicBackground.enabled = true;
		}
		else
		{
			PlayerPrefs.SetInt("music", 0);
			toggleMusicBackground.enabled = false;
		}
		//Sound
		if (!tuggleSoundButton.isOn)
		{
			PlayerPrefs.SetInt("sound", 1);
			toggleSoundBackground.enabled = true;
		}
		else
		{
			PlayerPrefs.SetInt("sound", 0);
			toggleSoundBackground.enabled = false;
		}
	}
}
